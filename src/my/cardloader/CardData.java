/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.cardloader;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import javax.smartcardio.Card;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.CommandAPDU;
import javax.smartcardio.ResponseAPDU;
import javax.smartcardio.TerminalFactory;

/**
 *
 * @author Glenn
 */
public class CardData {
    
    // Address block for each data on the card
    final protected static byte idNumAddr = (byte)0x01;
    final protected static byte lastName1Addr = (byte)0x02;
    final protected static byte lastName2Addr = (byte)0x04;
    final protected static byte firstName1Addr = (byte)0x05;
    final protected static byte firstName2Addr = (byte)0x06;
    final protected static byte birthdayAddr = (byte)0x08;
    final protected static byte cardBalAddr = (byte)0x09;
    final protected static byte lastTrans1Addr = (byte)0x0a;
    final protected static byte lastTrans2Addr = (byte)0x0c;
    final protected static byte transPrevAddr = (byte)0x0d;
    final protected static byte transCurrAddr = (byte)0x0e;
    final protected static byte accTypeAddr = (byte)0x10;
    final protected static byte sig1Addr = (byte)0x11;
    final protected static byte sig2Addr = (byte)0x12;
            
    final protected static String imageLocation = "C:\\\\Users\\\\Glenn\\\\BCDS3Projects\\\\POS\\\\Photos\\\\";
    final protected static String fileExtension = ".jpg";
    final protected static String key = "MoonAndBack3729";
    
    public static boolean checkIdNum(Connection con, String idNum)
    {
        boolean bool = false;
        Statement s = null;
        String conditions = idNum;
        
        String query = "SELECT * FROM `CUSTOMERS` WHERE `TAXID` = " + conditions;
        
        try{
            s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            
            if(rs.next())
            {
                bool = true;
            }
                
        }
        catch(SQLException e)
        {
            System.out.println(e.toString());
        }
        
        return bool;
    }
    
    public static boolean checkCardNum(Connection con, String cardNum)
    {
        boolean bool = false;
        Statement s = null;
        String conditions = cardNum;
        
        String query = "SELECT * FROM `CUSTOMERS` WHERE `CARD` = '" + conditions + "'";
        System.out.println("check card query = " + query);
        
        try{
            s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            
            if(rs.next())
            {
                bool = true;
            }
                
        }
        catch(SQLException e)
        {
            System.out.println(e.toString());
        }
        System.out.println("bool = " + bool);
        return bool;
    }
    
    public static String getID(Connection con, String cardNum)
    {
        Statement s = null;
        String conditions = cardNum;
        String idString = null;
        
        String query = "SELECT `TAXID` FROM `CUSTOMERS` WHERE `CARD` = '" + conditions + "'";
        System.out.println(query);
        
        try{
            s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            while(rs.next())
            {
                idString = rs.getString("TAXID");
                System.out.println(idString);
            }
        }
            catch(SQLException e)
                    {
                    System.out.println(e.toString());
                    }
        return idString;
    }
    
    public static String getAccType(Connection con, String idNum)
    {
        Statement s = null;
        String conditions = idNum;
        String taxCategory = null;
        
        String query = "SELECT `TAXCATEGORY` FROM `CUSTOMERS` WHERE `TAXID` =" + conditions;
        
        try{
            s=con.createStatement();
            ResultSet rs = s.executeQuery(query);
                while(rs.next())
                {
                    taxCategory = rs.getString("TAXCATEGORY");                    
                    if(taxCategory == null)
                    {
                        taxCategory = "Unknown Type!";
                    }
                    else if (taxCategory.equals("a427dfae-5b5a-4504-861e-272239581a0e")) {
                        taxCategory = "AMPC Member";
                    }
                    else if (taxCategory.equals("fd1e24af-76a2-4d24-977d-39ce04537114")) {
                        taxCategory = "Cash Card";
                    }
                    else if (taxCategory.equals("994643d2-fdf5-4bb8-bd21-6877402684c7")) {
                        taxCategory = "Scholar";
                    }
                    System.out.println(taxCategory);
                }
        }
        catch(SQLException e)
        {
            System.out.println(e.toString());
            System.out.println("Customer Data insufficient");
            taxCategory = "Database error!";
        }
        return taxCategory;
    }
    
    public static String getCardBal(Connection con, String idNum)
    {
        Statement s = null;
        String conditions = idNum;
        String curBal = null;
        
        String query = "SELECT `FAX` FROM `CUSTOMERS` WHERE `TAXID` =" +conditions;
        
        try{
            s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            while(rs.next())
            {
                curBal = rs.getString("FAX");
                System.out.println(curBal);
            }
        }
            catch(SQLException e)
                    {
                    System.out.println(e.toString());
                    }
        return curBal;
    }
    //Gets ONLY the first name from the database.
    public static String getFirstName(Connection con, String idNum)
    {
        Statement s = null;
        //String args = " `LASTNAME`, `Birthday`";
        String conditions = idNum;
        String firstName = null;
        
        String query = "SELECT `FIRSTNAME` FROM `CUSTOMERS` WHERE `TAXID` =" + conditions; //database query
        
        try{
            s = con.createStatement();
            ResultSet rs = s.executeQuery(query); //executes the query.
            
            while(rs.next())
            {
                firstName = rs.getString("FIRSTNAME"); //gets the result from the FIRSTNAME column
                System.out.println(firstName);
                
                if(firstName == null)
                {
                    firstName = "Name not found!";
                }
                
            }
        }
        catch(SQLException e)
        {
            System.out.println(e.toString()); //prints out the database error.
            firstName = "Name not found!";
        }
        finally{
            if (s != null) { try {
                s.close(); //ends the connection.
                } catch (SQLException ex) {
                    System.out.println(ex.toString());
                }
}
        }
        return firstName;
    }
   
    //similar to getFirstName method
   public static String getLastName(Connection con, String idNum)
    {
        Statement s = null;
        //String args = " `LASTNAME`, `Birthday`";
        String conditions = idNum;
        String lastName = null;
        
        String query = "SELECT `LASTNAME` FROM `CUSTOMERS` WHERE `TAXID` =" + conditions;
        
        try{
            s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            
            while(rs.next())
            {
                lastName = rs.getString("LASTNAME");
                System.out.println(lastName);
                if(lastName == null)
                {
                    lastName = "Name not found!";
                }
                
            }
        }
        catch(SQLException e)
        {
            System.out.println(e.toString());
            lastName = "Name not found!";
        }
        finally{
            if (s != null) { try {
                s.close();
                } catch (SQLException ex) {
                    System.out.println(ex.toString());
                }
                }
        }
        return lastName;
    }
   
    //similar to getFirstName method
    public static String getBirthday(Connection con, String idNum)
    {
        Statement s = null;
        //String args = " `LASTNAME`, `POSTAL`";
        String conditions = idNum;
        String birthDay = null;
        
        String query = "SELECT `POSTAL` FROM `CUSTOMERS` WHERE `TAXID` = " + conditions;
        
        try{
            s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            
            while(rs.next())
            {
                birthDay = rs.getString("POSTAL");
                System.out.println(birthDay);
                
                if(birthDay == null)
                {
                    birthDay = "Birthday not found!";
                }
            }
        }
        catch(SQLException e)
        {
            System.out.println("Luh:" + e.toString());
            //birthDay = "Birthday not found!";
            
        }
        finally{
            if (s != null) { 
                try {
                    s.close();
                } 
                catch (SQLException ex) {
                    System.out.println(ex.toString());
                }
            }
        }
        return birthDay;
    }
    
    // get last transaction date
    public static String getLastTransDate(Connection con, String idNum)
    {
        Statement s = null;        
        String conditions = idNum;
        String lastTransDate = null;
        
        String query = "SELECT `ADDRESS2` FROM `CUSTOMERS` WHERE `TAXID` = " + conditions;
        
        try{
            s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            
            while(rs.next())
            {
                lastTransDate = rs.getString("ADDRESS2");
                System.out.println(lastTransDate);
                
                if(lastTransDate == null)
                {
                    lastTransDate = "";
                }
            }
        }
        catch(SQLException e)
        {
            System.out.println("Luh:" + e.toString());
        }
        finally{
            if (s != null) { 
                try {
                    s.close();
                } 
                catch (SQLException ex) {
                    System.out.println(ex.toString());
                }
            }
        }
        return lastTransDate;
    }    

    // get total transaction amount from previous year
    public static String getTransPrev(Connection con, String idNum)
    {
        Statement s = null;        
        String conditions = idNum;
        String TransPrevYear = null;
        
        String query = "SELECT `REGION` FROM `CUSTOMERS` WHERE `TAXID` = " + conditions;
        
        try{
            s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            
            while(rs.next())
            {
                TransPrevYear = rs.getString("REGION");
                System.out.println(TransPrevYear);
                
                if(TransPrevYear == null)
                {
                    TransPrevYear = "";
                }
            }
        }
        catch(SQLException e)
        {
            System.out.println("Luh:" + e.toString());
        }
        finally{
            if (s != null) { 
                try {
                    s.close();
                } 
                catch (SQLException ex) {
                    System.out.println(ex.toString());
                }
            }
        }
        return TransPrevYear;
    }    
    
    // get total transaction amount from current year
    public static String getTransCurr(Connection con, String idNum)
    {
        Statement s = null;        
        String conditions = idNum;
        String TransCurrYear = null;
        
        String query = "SELECT `COUNTRY` FROM `CUSTOMERS` WHERE `TAXID` = " + conditions;
        
        try{
            s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            
            while(rs.next())
            {
                TransCurrYear = rs.getString("COUNTRY");
                System.out.println(TransCurrYear);
                
                if(TransCurrYear == null)
                {
                    TransCurrYear = "";
                }
            }
        }
        catch(SQLException e)
        {
            System.out.println("Luh:" + e.toString());
        }
        finally{
            if (s != null) { 
                try {
                    s.close();
                } 
                catch (SQLException ex) {
                    System.out.println(ex.toString());
                }
            }
        }
        return TransCurrYear;
    } 
    
    // get the hash signature saved on the card
    public static String getSignature()
    {
        String signature = "";
        try{
            signature = readByte(sig1Addr) + readByte(sig2Addr);
        }    
        catch (Exception e) {
            System.out.println("Ouch: " + e.toString());
        }
        System.out.println("card signature = " + signature);
        return signature;
    }
             
    public static String getCardMaxBal(Connection con, String accType)
    {
        Statement s = null;
        String conditions = accType;
        String maxCardBal = null;
        
        String query = "SELECT `MAXBAL` FROM `z_maxcardbal` WHERE `ACCTYPE` = '" +conditions + "'";
        
        try{
            s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            while(rs.next())
            {
                maxCardBal = rs.getString("MAXBAL");
                System.out.println(maxCardBal);
            }
        }
            catch(SQLException e)
                    {
                    System.out.println(e.toString());
                    }
        return maxCardBal;
    }
    
    // update database with cardnumber
    public static void updateDBCardNum(Connection con, String idNum, String cardNum)
    {
        Statement s = null;        
        String conditions = idNum;        
        String card = cardNum;
        
        String query = "UPDATE `CUSTOMERS` SET `CARD` = '" + card + "' WHERE `TAXID` = " + conditions + ";";
        System.out.print("query = ");
        System.out.println(query);
        try{
            s = con.createStatement();
            s.executeUpdate(query);          
        }
        catch(SQLException e)
        {
            System.out.println(e.toString());
        }
        finally{
            if (s != null) { try {
                s.close();
                } catch (SQLException ex) {
                    System.out.println(ex.toString());
                }
                }
        }
    }   
    
    //writes the ID number using writeBlockData  
    public static boolean writeIdNumber(String cardNumber) {
        int len = cardNumber.length();
        String cardNum;
        if (len > 16) {
            cardNum = cardNumber.substring(0, 16);
        } else if (len < 16) {
            cardNum = cardNumber;
            for (int i = len; i < 16; i++) {
                cardNum = cardNum + " ";
            }
        } else {
            cardNum = cardNumber;
        }
        return NFCmod.writeBlockData(cardNum, idNumAddr, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    
    //writes data LastName on the card
    public static boolean writeLastName(String lastName) {
        int len = lastName.length();
        String lastName1;
        String lastName2;
        if (len > 16) {
            lastName1 = lastName.substring(0, 16);
            lastName2 = lastName.substring(16);
        } else if (len < 16) {
            lastName1 = lastName;
            lastName2 = "                ";
            for (int i = len; i < 16; i++) {
                lastName1 = lastName1 + " ";
            }
        } else {
            lastName1 = lastName;
            lastName2 = "                ";
        }
        boolean ln1 = NFCmod.writeBlockData(lastName1, lastName1Addr, (byte)0x10, (byte)0x60, (byte)0x00);
        if (ln1 == false) {
            System.out.println("failed to write lastname1");
            return false;
        } else {
            boolean ln2 = NFCmod.writeBlockData(lastName2, lastName2Addr, (byte)0x10, (byte)0x60, (byte)0x00);
            if (ln2 == false) {
                System.out.println("failed to write lastname2");
            }
            return ln2;
        }
    }
    
    //writes the name using writeBlockData
    public static boolean writeFirstName(String firstName) {
        int len = firstName.length();
        String firstName1;
        String firstName2;
        if (len > 16) {
            firstName1 = firstName.substring(0, 16);
            firstName2 = firstName.substring(16);
            int len2 = firstName2.length();
            for (int j = len2; j < 16; j++) {
                firstName2 = firstName2 + " ";
            }
        } else if (len < 16) {
            firstName1 = firstName;
            firstName2 = "                ";
            for (int i = len; i < 16; i++) {
                firstName1 = firstName1 + " ";
            }
        } else {
            firstName1 = firstName;
            firstName2 = "                ";
        }
        boolean fn1 = NFCmod.writeBlockData(firstName1, firstName1Addr, (byte)0x10, (byte)0x60, (byte)0x00);
        if (fn1 == false) {
            System.out.println("failed to write firstname1");
            return false;
        } else {
            boolean ln2 = NFCmod.writeBlockData(firstName2, firstName2Addr, (byte)0x10, (byte)0x60, (byte)0x00);
            if (ln2 == false) {
                System.out.println("failed to write firstname2");
            }
            return ln2;
        }
    }
    
    //writes the birthday using writeBlockData
    public static boolean writeBirthday(String birthday) {
        int len = birthday.length();
        String bday;
        if (len > 16) {
            bday = birthday.substring(0, 16);
        } else if (len < 16) {
            bday = birthday;
            for (int i = len; i < 16; i++) {
                bday = bday + " ";
            }
        } else {
            bday = birthday;
        }
        return NFCmod.writeBlockData(bday, birthdayAddr, (byte)0x10, (byte)0x60, (byte)0x00);
    }
        
    //writes the current balance using writeBlockData
    public static boolean writeCardBalance(String cardBal) {
        int len = cardBal.length();
        String bal;
        if (len > 16) {
            bal = cardBal.substring(0, 16);
        } else if (len < 16) {
            bal = cardBal;
            for (int i = len; i < 16; i++) {
                bal = bal + " ";
            }
        } else {
            bal = cardBal;
        }
        return NFCmod.writeBlockData(bal, cardBalAddr, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    
    //writes the last transaction date using writeBlockData
    public static boolean writeLastTransDate(String lastTransDate) {
        int len = lastTransDate.length();
        String transDate1 = "";
        String transDate2 = "";
        if (len > 16) {
            transDate1 = lastTransDate.substring(0, 16);
            transDate2 = lastTransDate.substring(16);
            int len2 = transDate2.length();
            for (int j = len2; j < 16; j++) {
                transDate2 = transDate2 + " ";
            }
        } else if (len < 16) {
            transDate1 = lastTransDate;
            transDate2 = "                ";
            for (int i = len; i < 16; i++) {
                transDate1 = transDate1 + " ";
            }
        } else {
            transDate1 = lastTransDate;
            transDate2 = "                ";
        }
        boolean fn1 = NFCmod.writeBlockData(transDate1, lastTrans1Addr, (byte)0x10, (byte)0x60, (byte)0x00);
        if (fn1 == false) {
            System.out.println("failed to write transDate1");
            return false;
        } else {
            boolean ln2 = NFCmod.writeBlockData(transDate2, lastTrans2Addr, (byte)0x10, (byte)0x60, (byte)0x00);
            if (ln2 == false) {
                System.out.println("failed to write transDate2");
            }
            return ln2;
        }
    }
        
    //writes the total transaction for previous year using writeBlockData
    public static boolean writeTransPrevYear(String transPrev) {
        int len = transPrev.length();
        String prev;
        if (len > 16) {
            prev = transPrev.substring(0, 16);
        } else if (len < 16) {
            prev = transPrev;
            for (int i = len; i < 16; i++) {
                prev = prev + " ";
            }
        } else {
            prev = transPrev;
        }
        return NFCmod.writeBlockData(prev, transPrevAddr, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    
        //writes the total transaction for previous year using writeBlockData
    public static boolean writeTransCurrYear(String transCurr) {
        int len = transCurr.length();
        String curr;
        if (len > 16) {
            curr = transCurr.substring(0, 16);
        } else if (len < 16) {
            curr = transCurr;
            for (int i = len; i < 16; i++) {
                curr = curr + " ";
            }
        } else {
            curr = transCurr;
        }
        return NFCmod.writeBlockData(curr, transCurrAddr, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    
    public static boolean writeAccType(String accType) {
        int len = accType.length();
        String aType;
        if (len > 16) {
            aType = accType.substring(0, 16);
        } else if (len < 16) {
            aType = accType;
            for (int i = len; i < 16; i++) {
                aType = aType + " ";
            }
        } else {
            aType = accType;
        }
        return NFCmod.writeBlockData(aType, accTypeAddr, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    
    //writes the last transaction date using writeBlockData
    public static boolean writeHash(String hashcode) {
        int len = hashcode.length();
        String hashcode1 = "";
        String hashcode2 = "";
        if (len > 16) {
            hashcode1 = hashcode.substring(0, 16);
            hashcode2 = hashcode.substring(16);
            int len2 = hashcode2.length();
            for (int j = len2; j < 16; j++) {
                hashcode2 = hashcode2 + " ";
            }
        } else if (len < 16) {
            hashcode1 = hashcode;
            hashcode2 = "                ";
            for (int i = len; i < 16; i++) {
                hashcode1 = hashcode1 + " ";
            }
        } else {
            hashcode1 = hashcode;
            hashcode2 = "                ";
        }
        boolean fn1 = NFCmod.writeBlockData(hashcode1, sig1Addr, (byte)0x10, (byte)0x60, (byte)0x00);
        if (fn1 == false) {
            System.out.println("failed to write transDate1");
            return false;
        } else {
            boolean ln2 = NFCmod.writeBlockData(hashcode2, sig2Addr, (byte)0x10, (byte)0x60, (byte)0x00);
            if (ln2 == false) {
                System.out.println("failed to write transDate2");
            }
            return ln2;
        }
    }
    
    //reads data at the blockNumber
    public static String readByte(byte blockNumber)
    {
        String data = null;
        try {
            //Display the list of terminals
            TerminalFactory factory = TerminalFactory.getDefault();
            List<CardTerminal> terminals = factory.terminals().list();
            System.out.println("Terminals: " + terminals);

            // Use the first terminal
            CardTerminal terminal = terminals.get(0);

            // Connect with the card
            Card card = terminal.connect("*");
            System.out.println("card: " + card);
            CardChannel channel = card.getBasicChannel();
            
            // Load authentication 
            byte[] bytes2 = new byte[]{(byte) 0xFF, (byte) 0x82, (byte) 0x00, (byte) 0x00, (byte) 0x06, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF};
            ResponseAPDU answer = channel.transmit(new CommandAPDU(bytes2));
            System.out.println("answer:1 " + answer.toString());

            //Authenticate sector 0x01                                                                                  //change sector here
            byte[] authenticate = new byte[]{(byte) 0xFF, (byte) 0x86, (byte) 0x00, (byte) 0x00, (byte) 0x05, (byte) 0x01, (byte) 0x00, blockNumber, (byte) 0x60, (byte) 0x00};
            ResponseAPDU rs_authenticate = channel.transmit(new CommandAPDU(authenticate));
            System.out.println("answer:2 " + rs_authenticate.toString());

            //Read sector 0x01                                         //change sector
            byte[] read = new byte[]{(byte) 0xFF, (byte) 0xB0, (byte) 0x00, blockNumber, (byte) 0x10};
            ResponseAPDU rs_read = channel.transmit(new CommandAPDU(read));
            System.out.println("answer:ID Number " + rs_read.toString());
            byte[] byteArray = rs_read.getData();
            data = NFCmod.hexToString(NFCmod.bytesToHex(byteArray));
            //System.out.print(byteArray);
            System.out.println(data);
            return data;
        } catch (Exception e) {
            System.out.println("Ouch: " + e.toString());
        }
        return data;
    }
      
}
