/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package my.cardloader;;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.smartcardio.Card;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.CardTerminals;
import javax.smartcardio.CommandAPDU;
import javax.smartcardio.ResponseAPDU;
import javax.smartcardio.TerminalFactory;

/**
 *
 * @author Uskar
 */
public class NFCmod {
    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    
    public static String getCardId() {
        CardChannel channel = fetchReader();
        if (channel == null) {
            return null;
        } else {
            try {
                CommandAPDU command = new CommandAPDU(new byte[]{(byte)0xFF, (byte)0xCA, (byte)0x00, (byte)0x00, (byte)0x04});
                ResponseAPDU response = channel.transmit(command);
                byte[] byteArray = response.getData();
                return bytesToHex(byteArray);
            } catch (CardException e) {
                System.out.println(e.getMessage());
                return null;
            }
        }
    }
    public static boolean loadAuthentication(byte location) {
        CardChannel channel = fetchReader();
        if (channel == null) {
            return false;
        } else {
            try {
                byte[] bytes = new byte[]{(byte)0xFF, (byte)0x82, (byte)0x00, location, (byte)0x06, (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF};
                CommandAPDU command = new CommandAPDU(bytes);
                ResponseAPDU response = channel.transmit(command);
                int resp = response.getSW1();
                System.out.println("result:" + resp);
                if (resp == 144) {
                    return true;
                } else if (resp == 99) {
                    return false;
                } else {
                    return false;
                }
            } catch (CardException e) {
                System.out.println(e.getMessage());
                return false;
            }
        }
    }
    public static boolean authenticate(byte blockNumber, byte keyType, byte location) {
        CardChannel channel = fetchReader();
        if (channel == null) {
            return false;
        } else {
            try {
                byte[] bytes = new byte[]{(byte)0xFF, (byte)0x86, (byte)0x00, (byte)0x00, (byte)0x05, (byte)0x01, (byte)0x00, blockNumber, keyType, location};
                CommandAPDU command = new CommandAPDU(bytes);
                ResponseAPDU response = channel.transmit(command);
                int resp = response.getSW1();
                System.out.println("result:" + resp);
                if (resp == 144) {
                    return true;
                } else if (resp == 99) {
                    return false;
                } else {
                    return false;
                }
            } catch (CardException e) {
                System.out.println(e.getMessage());
                return false;
            }
        }
    }
    public static String readBlockData(byte blockNumber, byte numberOfBytes, byte keyType, byte location) {
        boolean loadAuthentication = loadAuthentication(location);
        if (loadAuthentication == false) {
            System.out.println("failed to load authentication");
            return null;
        }
        boolean authenticate = authenticate(blockNumber, keyType, location);
        if (authenticate == false) {
            System.out.println("failed to authenticate");
            return null;
        }
        CardChannel channel = fetchReader();
        if (channel == null) {
            return null;
        } else {
            try {
                byte[] bytes = new byte[]{(byte)0xFF, (byte)0xB0, (byte)0x00, blockNumber, numberOfBytes};
                CommandAPDU command = new CommandAPDU(bytes);
                ResponseAPDU response = channel.transmit(command);
                int resp = response.getSW1();
                if (resp == 144) {
                    byte[] byteArray = response.getData();
                    String data = hexToString(bytesToHex(byteArray));
                    return data;
                } else {
                    System.out.println("operation failed");
                    return null;
                }
            } catch (CardException e) {
                System.out.println(e.getMessage());
                return null;
            }
        }
    }
    public static boolean writeBlockData(String data, byte blockNumber, byte numberOfBytes, byte keyType, byte location) {
        boolean loadAuthentication = loadAuthentication(location);
        if (loadAuthentication == false) {
            System.out.println("failed to load authentication");
            return false;
        }
        boolean authenticate = authenticate(blockNumber, keyType, location);
        if (authenticate == false) {
            System.out.println("failed to authenticate");
            return false;
        }
        CardChannel channel = fetchReader();
        if (channel == null) {
            System.out.println("no channel found");
            return false;
        } else {
            try {
                char[] chars = data.toCharArray();
                int total = chars.length + 5;
                byte[] bytes;
                bytes = new byte[total];
                bytes[0] = (byte)0xFF;
                bytes[1] = (byte)0xD6;
                bytes[2] = (byte)0x00;
                bytes[3] = blockNumber;
                bytes[4] = numberOfBytes;
                for (int k = 0; k < chars.length; k++) {
                    bytes[k + 5] = (byte) chars[k];
                }
                CommandAPDU command = new CommandAPDU(bytes);
                ResponseAPDU response = channel.transmit(command);
                int resp = response.getSW1();
                System.out.println(resp);
                return resp == 144;
            } catch (CardException e) {
                System.out.println(e.getMessage());
                return false;
            }
        }
    }
    public static String readIdNumber() {
        return readBlockData((byte)0x01, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static String readLastName() {
        String lastName1 = readBlockData((byte)0x02, (byte)0x10, (byte)0x60, (byte)0x00);
        String lastName2 = readBlockData((byte)0x04, (byte)0x10, (byte)0x60, (byte)0x00);
        return (lastName1 + lastName2).trim();
    }
    public static String readFirstName() {
        String firstName1 = readBlockData((byte)0x05, (byte)0x10, (byte)0x60, (byte)0x00);
        String firstName2 = readBlockData((byte)0x06, (byte)0x10, (byte)0x60, (byte)0x00);
        return (firstName1 + firstName2).trim();
    }
    public static String readBirthday() {
        return readBlockData((byte)0x08, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static String readLastAccessDate() {
        return readBlockData((byte)0x09, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static String readService01LastAccessDate() {
        return readBlockData((byte)0x0a, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static String readService02LastAccessDate() {
        return readBlockData((byte)0x0c, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static String readService03LastAccessDate() {
        return readBlockData((byte)0x0d, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static String readService04LastAccessDate() {
        return readBlockData((byte)0x0e, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static String readService05LastACcessDate() {
        return readBlockData((byte)0x10, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static String readService06LastAccessDate() {
        return readBlockData((byte)0x11, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static String readService07LastAccessDate() {
        return readBlockData((byte)0x12, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static String readService08LastAccessDate() {
        return readBlockData((byte)0x14, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static String readService09LastAccessDate() {
        return readBlockData((byte)0x15, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static String readService10LastAccessDate() {
        return readBlockData((byte)0x16, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static String readLastAccessedService() {
        return readBlockData((byte)0x18, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static String readSignaturePadding() {
        return readBlockData((byte)0x3c, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static String readSignaturePadding2() {
        return readBlockData((byte)0x3d, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static boolean writeIdNumber(String cardNumber) {
        int len = cardNumber.length();
        String cardNum;
        if (len > 16) {
            cardNum = cardNumber.substring(0, 16);
        } else if (len < 16) {
            cardNum = cardNumber;
            for (int i = len; i < 16; i++) {
                cardNum = cardNum + " ";
            }
        } else {
            cardNum = cardNumber;
        }
        return writeBlockData(cardNum, (byte)0x01, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static boolean writeLastName(String lastName) {
        int len = lastName.length();
        String lastName1;
        String lastName2;
        if (len > 16) {
            lastName1 = lastName.substring(0, 16);
            lastName2 = lastName.substring(16);
        } else if (len < 16) {
            lastName1 = lastName;
            lastName2 = "                ";
            for (int i = len; i < 16; i++) {
                lastName1 = lastName1 + " ";
            }
        } else {
            lastName1 = lastName;
            lastName2 = "                ";
        }
        boolean ln1 = writeBlockData(lastName1, (byte)0x02, (byte)0x10, (byte)0x60, (byte)0x00);
        if (ln1 == false) {
            System.out.println("failed to write lastname1");
            return false;
        } else {
            boolean ln2 = writeBlockData(lastName2, (byte)0x04, (byte)0x10, (byte)0x60, (byte)0x00);
            if (ln2 == false) {
                System.out.println("failed to write lastname2");
            }
            return ln2;
        }
    }
    public static boolean writeFirstName(String firstName) {
        int len = firstName.length();
        String firstName1;
        String firstName2;
        if (len > 16) {
            firstName1 = firstName.substring(0, 16);
            firstName2 = firstName.substring(16);
            int len2 = firstName2.length();
            for (int j = len2; j < 16; j++) {
                firstName2 = firstName2 + " ";
            }
        } else if (len < 16) {
            firstName1 = firstName;
            firstName2 = "                ";
            for (int i = len; i < 16; i++) {
                firstName1 = firstName1 + " ";
            }
        } else {
            firstName1 = firstName;
            firstName2 = "                ";
        }
        boolean fn1 = writeBlockData(firstName1, (byte)0x05, (byte)0x10, (byte)0x60, (byte)0x00);
        if (fn1 == false) {
            System.out.println("failed to write firstname1");
            return false;
        } else {
            boolean ln2 = writeBlockData(firstName2, (byte)0x06, (byte)0x10, (byte)0x60, (byte)0x00);
            if (ln2 == false) {
                System.out.println("failed to write firstname2");
            }
            return ln2;
        }
    }
    public static boolean writeBirthday(String birthday) {
        int len = birthday.length();
        String bday;
        if (len > 16) {
            bday = birthday.substring(0, 16);
        } else if (len < 16) {
            bday = birthday;
            for (int i = len; i < 16; i++) {
                bday = bday + " ";
            }
        } else {
            bday = birthday;
        }
        return writeBlockData(bday, (byte)0x08, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static boolean writeLastAccessDate(String lastAccessDate) {
        int len = lastAccessDate.length();
        String accessDate;
        if (len > 16) {
            accessDate = lastAccessDate.substring(0, 16);
        } else if (len < 16) {
            accessDate = lastAccessDate;
            for (int i = len; i < 16; i++) {
                accessDate = accessDate + " ";
            }
        } else {
            accessDate = lastAccessDate;
        }
        return writeBlockData(accessDate, (byte)0x09, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static boolean writeService01Date(String date) {
        int len = date.length();
        String accessDate;
        if (len > 16) {
            accessDate = date.substring(0, 16);
        } else if (len < 16) {
            accessDate = date;
            for (int i = len; i < 16; i++) {
                accessDate = accessDate + " ";
            }
        } else {
            accessDate = date;
        }
        return writeBlockData(accessDate, (byte)0x0a, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static boolean writeService02Date(String date) {
        int len = date.length();
        String accessDate;
        if (len > 16) {
            accessDate = date.substring(0, 16);
        } else if (len < 16) {
            accessDate = date;
            for (int i = len; i < 16; i++) {
                accessDate = accessDate + " ";
            }
        } else {
            accessDate = date;
        }
        return writeBlockData(accessDate, (byte)0x0c, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static boolean writeService03Date(String date) {
        int len = date.length();
        String accessDate;
        if (len > 16) {
            accessDate = date.substring(0, 16);
        } else if (len < 16) {
            accessDate = date;
            for (int i = len; i < 16; i++) {
                accessDate = accessDate + " ";
            }
        } else {
            accessDate = date;
        }
        return writeBlockData(accessDate, (byte)0x0d, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static boolean writeService04Date(String date) {
        int len = date.length();
        String accessDate;
        if (len > 16) {
            accessDate = date.substring(0, 16);
        } else if (len < 16) {
            accessDate = date;
            for (int i = len; i < 16; i++) {
                accessDate = accessDate + " ";
            }
        } else {
            accessDate = date;
        }
        return writeBlockData(accessDate, (byte)0x0e, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static boolean writeService05Date(String date) {
        int len = date.length();
        String accessDate;
        if (len > 16) {
            accessDate = date.substring(0, 16);
        } else if (len < 16) {
            accessDate = date;
            for (int i = len; i < 16; i++) {
                accessDate = accessDate + " ";
            }
        } else {
            accessDate = date;
        }
        return writeBlockData(accessDate, (byte)0x10, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static boolean writeService06Date(String date) {
        int len = date.length();
        String accessDate;
        if (len > 16) {
            accessDate = date.substring(0, 16);
        } else if (len < 16) {
            accessDate = date;
            for (int i = len; i < 16; i++) {
                accessDate = accessDate + " ";
            }
        } else {
            accessDate = date;
        }
        return writeBlockData(accessDate, (byte)0x11, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static boolean writeService07Date(String date) {
        int len = date.length();
        String accessDate;
        if (len > 16) {
            accessDate = date.substring(0, 16);
        } else if (len < 16) {
            accessDate = date;
            for (int i = len; i < 16; i++) {
                accessDate = accessDate + " ";
            }
        } else {
            accessDate = date;
        }
        return writeBlockData(accessDate, (byte)0x12, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static boolean writeService08Date(String date) {
        int len = date.length();
        String accessDate;
        if (len > 16) {
            accessDate = date.substring(0, 16);
        } else if (len < 16) {
            accessDate = date;
            for (int i = len; i < 16; i++) {
                accessDate = accessDate + " ";
            }
        } else {
            accessDate = date;
        }
        return writeBlockData(accessDate, (byte)0x14, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static boolean writeService09Date(String date) {
        int len = date.length();
        String accessDate;
        if (len > 16) {
            accessDate = date.substring(0, 16);
        } else if (len < 16) {
            accessDate = date;
            for (int i = len; i < 16; i++) {
                accessDate = accessDate + " ";
            }
        } else {
            accessDate = date;
        }
        return writeBlockData(accessDate, (byte)0x15, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static boolean writeService10Date(String date) {
        int len = date.length();
        String accessDate;
        if (len > 16) {
            accessDate = date.substring(0, 16);
        } else if (len < 16) {
            accessDate = date;
            for (int i = len; i < 16; i++) {
                accessDate = accessDate + " ";
            }
        } else {
            accessDate = date;
        }
        return writeBlockData(accessDate, (byte)0x16, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static boolean writeLastAccessedService(String service) {
        int len = service.length();
        String lastAccessed;
        if (len > 16) {
            lastAccessed = service.substring(0, 16);
        } else if (len < 16) {
            lastAccessed = service;
            for (int i = len; i < 16; i++) {
                lastAccessed = lastAccessed + " ";
            }
        } else {
            lastAccessed = service;
        }
        return writeBlockData(lastAccessed, (byte)0x18, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static boolean writeSignaturePadding(String signaturePad) {
        int len = signaturePad.length();
        String pad;
        if (len > 16) {
            pad = signaturePad.substring(0, 16);
        } else if (len < 16) {
            pad = signaturePad;
            for (int i = len; i < 16; i++) {
                pad = pad + " ";
            }
        } else {
            pad = signaturePad;
        }
        return writeBlockData(pad, (byte)0x3c, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    public static boolean writeSignaturePadding2(String signaturePad) {
        int len = signaturePad.length();
        String pad;
        if (len > 16) {
            pad = signaturePad.substring(0, 16);
        } else if (len < 16) {
            pad = signaturePad;
            for (int i = len; i < 16; i++) {
                pad = pad + " ";
            }
        } else {
            pad = signaturePad;
        }
        return writeBlockData(pad, (byte)0x3d, (byte)0x10, (byte)0x60, (byte)0x00);
    }
    
    public static CardChannel fetchReader() {
        TerminalFactory factory;
        CardTerminals terminals;
        List<CardTerminal> terminalList;
        CardTerminal terminal;
        Card card;
        CardChannel channel;
        try {
            factory = TerminalFactory.getInstance("PC/SC", null);
            terminals = factory.terminals();
            terminalList = terminals == null ? null : terminals.list();
            if (terminalList == null) {
                return null;
            } else {
                terminal = terminalList.get(0);
                card = terminal.connect("*");
//                System.out.println("card: " + card);
                channel = card.getBasicChannel();
                return channel;
            }
        } catch (NoSuchAlgorithmException | CardException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
    
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for ( int j = 0; j < bytes.length; j++ ) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
    public static String stringToHex(String str){
        char[] chars = str.toCharArray();
        StringBuffer hex = new StringBuffer();
        for(int i = 0; i < chars.length; i++){
            hex.append(Integer.toHexString((int)chars[i]));
        }
        return hex.toString();
    }
    public static String hexToString(String hex){
        StringBuilder sb = new StringBuilder();
        StringBuilder temp = new StringBuilder();

        //49204c6f7665204a617661 split into two characters 49, 20, 4c...
        for (int i = 0; i < hex.length() - 1; i += 2 ){
            //grab the hex in pairs
            String output = hex.substring(i, (i + 2));
            //convert hex to decimal
            int decimal = Integer.parseInt(output, 16);
            //convert the decimal to character
            sb.append((char)decimal);
            temp.append(decimal);
        }
        return sb.toString();
    }
    public static String generate3cData(String toHash) {
        String hashStr = toHash;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(hashStr.getBytes("UTF-8"));
            String encryptedHexStr = bytesToHex(hash);
            int len = encryptedHexStr.length();
            return encryptedHexStr.substring(len - 16, len);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
    public static boolean verify3cData(String cardId, String idNumber, String lastName, String firstName, String birthday, String lastAccessDate, 
            String secret, String string3c) {
        String concat = cardId.trim() + idNumber.trim() + lastName.trim() + firstName.trim() + birthday.trim() + lastAccessDate.trim() + secret;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(concat.getBytes("UTF-8"));
            String encryptedHexStr = bytesToHex(hash);
            int len = encryptedHexStr.length();
            String toCheck = encryptedHexStr.substring(len - 16, len);
            return toCheck.equals(string3c);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }
    public static String generate3dData(String toHash) {
        String hashStr = toHash;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(hashStr.getBytes("UTF-8"));
            String encryptedHexStr = bytesToHex(hash);
            int len = encryptedHexStr.length();
            return encryptedHexStr.substring(len - 32, len - 16);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
    public static boolean verify3dData(String cardId, String idNumber, String lastName, String firstName, String birthday, String lastAccessDate,
            String secret, String string3b) {
        String concat = cardId.trim() + idNumber.trim() + lastName.trim() + firstName.trim() + birthday.trim() + lastAccessDate.trim() + secret;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(concat.getBytes("UTF-8"));
            String encryptedHexStr = bytesToHex(hash);
            int len = encryptedHexStr.length();
            String toCheck = encryptedHexStr.substring(len - 32, len - 16);
            return toCheck.equals(string3b);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }
}
