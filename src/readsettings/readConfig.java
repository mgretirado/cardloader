/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readsettings;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Silerio
 */
public class readConfig {
       
    public static String[] readSettings() throws FileNotFoundException, IOException {
        
        BufferedReader in;
        in = new BufferedReader(new FileReader("C:\\Program Files (x86)\\unicentaopos-3.91.3\\Settings.txt"));
        String str;

        List<String> list = new ArrayList<String>();
        int i= 0;
        while((str = in.readLine()) != null){
            list.add(str);
            i++;
        }

        String[] stringArr = list.toArray(new String[0]);
        //System.out.println(i);
        System.out.println("reading settings.. ");
        System.out.println(stringArr[0]); // IP name 
        System.out.println(stringArr[1]); // username
        System.out.println(stringArr[2]); // password 
        System.out.println(stringArr[3]); // database name 
        System.out.println(stringArr[4]); // image location
        
        String ip = stringArr[0];
        String username = stringArr[1];
        String password = stringArr[2];
        String database = stringArr[3];
        String image = stringArr[4];
        
        ip = ip.replace("ip:","");
        username = username.replace("username:", "");
        password = password.replace("password:","");
        database = database.replace("database name:","");
        image = image.replace("image location:", "");
        
        
        String arrSettings[] = new String[5];
        arrSettings[0] = ip.trim();
        arrSettings[1] = username.trim();
        arrSettings[2] = password.trim();
        arrSettings[3] = database.trim();
        arrSettings[4]=  image.trim();
        
        
        return arrSettings;
    }
}
