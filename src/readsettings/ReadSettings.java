
package readsettings;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Silerio
 */
public class ReadSettings {

      static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
      //static final String DB_URL = "jdbc:mysql://localhost/dummy_database";
      static String DB_URL, USER, PASS;

      
    public static Connection con()  {

        Connection conn = null;
        Statement stmt = null;
        String settings[] = null;
          try {
              settings = readIp.readSettings();
          } catch (IOException ex) {
              Logger.getLogger(ReadSettings.class.getName()).log(Level.SEVERE, null, ex);
          }
        
        DB_URL = "jdbc:mysql://" + settings[0] + "/" + settings[3];
        USER = settings[1];
        PASS = settings[2];
        
       
         try{
      //STEP 2: Register JDBC driver
      Class.forName("com.mysql.jdbc.Driver");
    
      //STEP 3: Open a connection
      System.out.println("Connecting to database...");

         conn = DriverManager.getConnection(DB_URL,USER,PASS);
            PreparedStatement pst=null;
            ResultSet rse=null;
    
      System.out.println("executed...");
         
         } catch(Exception e){
      //Handle errors for Class.forName
      e.printStackTrace();      
        }
return conn;
    }
}
    

